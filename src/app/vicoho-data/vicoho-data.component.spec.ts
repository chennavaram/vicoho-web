import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VicohoDataComponent } from './vicoho-data.component';

describe('VicohoDataComponent', () => {
  let component: VicohoDataComponent;
  let fixture: ComponentFixture<VicohoDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VicohoDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VicohoDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
