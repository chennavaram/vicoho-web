import { TestBed, inject } from '@angular/core/testing';

import { VicohoDataService } from './vicoho-data.service';

describe('VicohoDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VicohoDataService]
    });
  });

  it('should be created', inject([VicohoDataService], (service: VicohoDataService) => {
    expect(service).toBeTruthy();
  }));
});
