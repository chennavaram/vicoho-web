import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { VicohoDataService } from './vicoho-data/vicoho-data.service';

import { AppComponent } from './app.component';
import { VicohoDataComponent } from './vicoho-data/vicoho-data.component';

@NgModule({
  declarations: [
    AppComponent,
    VicohoDataComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [VicohoDataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
