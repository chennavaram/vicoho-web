export interface VicohoData {
    id: Number;
    name: String;
    city: String;
    employeeId: String;
    status: String;
  }