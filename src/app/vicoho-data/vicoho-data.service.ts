import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VicohoDataService {
  constructor(private http: HttpClient) { }
  url = 'http://localhost:4000';
  getData() {
    return this
            .http
            .get(`${this.url}/employees`);
        }
}
