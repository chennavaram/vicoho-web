import { Component, OnInit } from '@angular/core';
import { VicohoDataService } from './vicoho-data.service';
import { VicohoData } from './vicoho-data';
@Component({
  selector: 'app-vicoho-data',
  templateUrl: './vicoho-data.component.html',
  styleUrls: ['./vicoho-data.component.css']
})
export class VicohoDataComponent implements OnInit {
  employees: VicohoData[];
  constructor(private vicohoservice: VicohoDataService) { }

  ngOnInit() {
    this.vicohoservice.getData().subscribe((data: VicohoData[]) => {
      this.employees = data;
    });
  }

}
