import { Component } from '@angular/core';
//import { VicohoDataComponent } from './vicoho-data/vicoho-data.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ViCoHo-Web';
}
